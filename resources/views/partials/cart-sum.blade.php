<div class="cart">
    <div class="cart__counter">
        <span class="cart__circle cart--circle">
            <img src="{{ asset('images/cart-bag.svg') }}" alt="{{ __('Kraunasi..') }}">

            <span class="cart__sum">
                <span class="cart__number">0</span>
            </span>
        </span>
    </div>
</div>
