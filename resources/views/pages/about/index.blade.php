@extends('layouts.app')

@section('title', $title)

@section('content')

    <section class="section page">
        <div class="container">
            <h2 class="section__title section--title">
                {{ $title }}
            </h2>

            <div class="page__content">
                {!! $body !!}
            </div>
        </div>
    </section>

@endsection