<section class="section social social--bg">
    <div class="container">
        <div class="social__block">
            <div class="social__box">
                <div class="social__box-border">
                    <p class="social__text">
                        @php
                            $siteInstagram = \App\Helpers\Settings::config('site_instagram');
                        @endphp

                        {!! __("Instagram: <strong>{$siteInstagram}</strong>") !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>