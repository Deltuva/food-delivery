<footer class="footer">
    <div class="container">
        <div class="footer__bottom">
            <div class="footer__company">
                &copy; {!! \App\Helpers\Settings::config('site_footer_company') !!}
            </div>
            <div class="footer__logo">
                <a href="{{ route('app.home') }}">
                    <img class="logo" src="{{ asset('images/' . \App\Helpers\Settings::config('site_logo')) }}"
                        alt="Logo">
                </a>
            </div>
            <div class="footer__copyright">
                Made by <strong>{!! \App\Helpers\Settings::config('site_footer_created_by') !!}</strong>
            </div>
        </div>
    </div>
</footer>
