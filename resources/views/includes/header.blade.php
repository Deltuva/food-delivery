<header class="header header--bg">
    <div class="container">
        <div class="header__top">
            <button class="mobile-button">
                <span class="mobile-button__line"></span>
                <span class="mobile-button__line"></span>
                <span class="mobile-button__line"></span>
            </button>
            <nav class="topmnu">
                @foreach ($pages as $page)
                    <a href="{{ url($page->slug) }}" class="topmnu__link">{{ $page->title }}</a>
                @endforeach
            </nav>
            @php
                $siteLogo = asset('images/' . \App\Helpers\Settings::config('site_logo'));
            @endphp

            <a href="{{ route('app.home') }}">
                <img class="logo" src="{{ $siteLogo }}" alt="Logo">
            </a>

            <div class="contact">
                <span class="contact__workhours">
                    @php
                        $siteWorkHours = \App\Helpers\Settings::config('site_work_hours');
                    @endphp

                    {{ __('Darbo laikas.') }} {{ $siteWorkHours }}
                </span>
                <span class="contact__phone">
                    <img class="callphone" src="{{ asset('images/call.svg') }}" alt="Call us">
                    <div class="number">
                        @php
                            $sitePhoneNumber = \App\Helpers\Settings::config('site_phone_number');
                        @endphp

                        <a href="tel:+{{ $sitePhoneNumber }}"> {{ $sitePhoneNumber }}</a>
                    </div>
                </span>
            </div>
        </div>
        <div class="hero">
            <div class="container">
                <h1 class="hero__title">
                    @php
                        $siteHeroTitle = \App\Helpers\Settings::config('site_hero_title');
                    @endphp

                    {{ $siteHeroTitle }}
                </h1>
                <h2 class="hero__subtitle">
                    @php
                        $siteHeroSubTitle = \App\Helpers\Settings::config('site_hero_subtitle');
                    @endphp
                    
                    {!! $siteHeroSubTitle !!}
                </h2>
                <div class="pizza__block">
                    <div class="tooltip">
                        <div class="tooltip__hover plus"></div>
                        <span class="pizza__tooltip">
                            <span class="pizza__tooltip--price">
                                @php
                                    $freeDeliveryPrice = \App\Helpers\Settings::config('free_delivery');
                                @endphp

                                {!! __("Užsakant už <strong>{$freeDeliveryPrice} &euro;</strong><br /> pristatymas nemokamas</span>") !!}
                            </span>
                    </div>
                    <img src="{{ asset('images/pizza.svg') }}" alt="Pizza">
                </div>
            </div>
        </div>
    </div>
</header>
