@extends('layouts.app')

@section('title', config('app.name'))

@section('content')

    <main role="main">
        @include('sections.products')
        @include('sections.social')
        @include('sections.call')
    </main>

@endsection
