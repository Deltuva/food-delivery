@if (session()->has('message'))
    <div class="call__success">
        {{ session('message') }}
    </div>
@endif
