<section class="section call call--bg">
    <div class="container">
        <div class="call__block">
            <h2 class="section__title section--title">
                {!! __('Užpildykite formą<br />
                ir mes jums paskambinsime ') !!}
            </h2>
            @include('livewire.partials.success')

            <div class="call__form">
                <form wire:submit.prevent="callSaveRequest">
                    <input type="text" wire:model="call.name" class="call__input" id="form-name"
                        placeholder="{{ __('Jūsų vardas') }}">

                    <input type="text" wire:model="call.phone_no" class="call__input" id="form-phone_number"
                        placeholder="{{ __('Telefonas') }}">

                    <button type="submit" class="call__btn">
                        {{ __('Registruoti') }} <i wire:loading.remove wire:target="callSaveRequest" class="call-ico"></i>

                        <div wire:loading wire:target="callSaveRequest">
                            <img class="call__loader" src="{{ asset('images/loader.svg') }}" alt="{{ __('Loaderis..') }}">
                        </div>
                    </button>
                </form>
            </div>

            <div class="car__block">
                <img src="{{ asset('images/car.svg') }}" alt="Car">
            </div>
        </div>
    </div>
</section>
