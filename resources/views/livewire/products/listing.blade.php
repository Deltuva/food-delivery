<section class="section food food--bg">
    <div class="container">
        <h2 class="section__title section--title">
            {{ __('Maisto meniu') }}
        </h2>

        <nav class="food-categories__nav food-categories--nav">
            @foreach ($categories as $category)
                <li wire:click="selectCategoryId({{ $category->id() }})" class="food-categories__nav-link
                    @if ($selectedCategoryId===$category->id()) active @endif">
                    {{ $category->name }}
                </li>
            @endforeach

            <li wire:click="selectAll" class="food-categories__nav-link 
            @if (!$selectedCategoryId) active @endif">
                {{ __('Visi') }}
            </li>
        </nav>

        <div class="food-block">
            @forelse ($products as $product)
                <div class="food-block__item food-block--item">
                    @php
                        $top = $product->isRecommend();
                        $discounted = $product->isDiscounted();
                        $discountPrice = $product->getDiscountForPrice();
                    @endphp

                    {!! $top ? '<span class="is-top">top</span>' : '' !!}
                    {!! $discounted ? '<span class="is-discounted">akcija</span>' : '' !!}

                    <img src="{{ $product->photoImage() }}" class="food-block__image">

                    <div class="food-block__desc">
                        <div class="top">
                            <span class="title">
                                @isset($product->title)
                                    {{ $product->title }}
                                @endisset
                            </span>
                            <span class="size">
                                @isset($product->size->size)
                                    {{ $product->size->size }}
                                @endisset
                            </span>
                        </div>
                        <p class="ingridients">
                            @isset($product->description)
                                {{ $product->description }}
                            @endisset
                        </p>
                        <strong class="price">
                            @php
                                $currencyPrefix = '&euro;';
                            @endphp

                            @if($product->original_price)
                                {{ $discountPrice }} {!! $currencyPrefix !!}
                                @if ($discounted)
                                    <span class='price__discount'>
                                        {{ $product->getOriginalPrice() }} {!! $currencyPrefix !!}
                                    </span>
                                @endif
                            @endif
                        </strong>
                        <button class="add">
                            <i class="cart-ico"></i> {{ __('Į krepšelį') }}
                        </button>
                    </div>
                </div>
            @empty
                <div class="food__empty">
                    <img src="{{ asset('images/restaurant-empty.svg') }}" alt="{{ __('Meniu') }}">
                    <div class="food__empty-text">
                        {{ __('Nėra nieko mūsų meniu.') }}
                    </div>
                </div>
            @endforelse
        </div>

        @if ($products->hasMorePages())
            <div class="more-block">
                <button wire:loading.remove wire:click="fetchMore" class="btn">
                    {{ $product->categoryId() === 5 ? __('Daugiau gėrimų') : __('Daugiau maisto') }}
                </button>
                <div wire:loading wire:target="fetchMore">
                    <div class="loader">
                        <div class="loader__circle">
                            <img src="{{ asset('images/loader.svg') }}" alt="{{ __('Loaderis..') }}">
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>
