<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">

  <title>@yield('title')</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <!--[if lt IE 9]>
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
  <![endif]-->

  <meta name="application-name" content="Food Delivery App" />
  <meta name="msapplication-TileColor" content="#FD9924" />

  @livewireStyles
  
</head>

<body>


  <div id="app">
    <!-- Top Header nav -->
    @include('includes.header')

    <!-- Right Cart Counter -->
    @include('partials.cart-sum')

    @yield('content')

    <!-- Bottom footer -->
    @include('includes.footer')
  </div>

  <!-- Scripts -->
  <script src="{{ mix('js/app.js') }}"></script>

  @livewireScripts

  @stack('scripts')
</body>

</html>
