<?php

namespace App\Helpers;

use App\Models\Setting;

//cache()->flush();

class Settings
{
    protected $settingModel;

    public static function config($key)
    {
        $settingModel = new Setting;
        $settings = $settingModel->where('key', $key)->first();
        $cache = cache()->has($key) ? cache()->get($key) : $settings;

        if (!is_null($cache)) {
            $forever = cache()->forever($settings->key, $settings->value);

            $key = $settings->getValue() == NULL ? $key : $forever;
            if ($key) {
                return $settings->getValue();
            }
            return $key;
        }
    }
}
