<?php

namespace App\Http\Controllers\Pages;

use App\Models\Page;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $pageModel = new Page;
        $page = $pageModel->whereSlug($pageModel::ABOUT_PAGE_SLUG)->active()->first();

        if (!$page) return;

        return view('pages.payment.index')->with([
            'title' => $page->title,
            'body'  => $page->body
        ]);
    }
}
