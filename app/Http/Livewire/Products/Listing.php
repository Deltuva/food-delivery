<?php

namespace App\Http\Livewire\Products;

use App\Models\Category;
use App\Models\Product;
use Livewire\Component;

class Listing extends Component
{
    protected $products;

    public $categories;
    public $selectedCategoryId;
    public $hasFetchMore = false;
    public $perPage      = 8;
    public $maxPerPage   = 0;
    public $categoryModel;
    public $productModel;

    public function mount(){
        $this->categoryModel = new Category;
        $this->productModel  = new Product;
    }

    public function selectCategoryId($categoryId)
    {
        $this->selectedCategoryId = $categoryId;
    }

    public function selectAll()
    {
        $this->selectedCategoryId = '';
    }

    public function fetchMore()
    {
        $this->maxPerPage = $this->productModel::PER_PAGE;
        $this->perPage = $this->perPage + $this->maxPerPage;
    }

    public function fetchCategories()
    {
        $this->categories = $this->categoryModel->all();
    }

    public function fetchProducts()
    {
        if (!$this->selectedCategoryId) {
            $this->products = $this->productModel
                ->active()->paginate($this->perPage);
        } else {
            $this->products = $this->productModel
                ->whereCategoryId($this->selectedCategoryId)->active()->paginate($this->perPage);
        }
        return $this->products;
    }

    public function render()
    {
        return view('livewire.products.listing', [
            'categories' => $this->fetchCategories(),
            'products'   => $this->fetchProducts()
        ]);
    }
}
