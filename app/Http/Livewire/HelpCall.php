<?php

namespace App\Http\Livewire;

use App\Models\Call;
use Livewire\Component;

class HelpCall extends Component
{
    public $call;
    public $callSuccess = false;

    protected $rules = [
        'call.name' => 'required|min:6',
        'call.phone_no' => 'required',
        'call.phone_no' => ['regex:/^(86|\+3706)([0-9]){3}([0-9]){4}/i']
    ];

    public function mount()
    {
        $this->call = new Call;
    }

    public function callSaveRequest()
    {
        $valid = $this->validate();

        if ($valid) {
            $this->call->create([
                'name'     => $this->call->name,
                'phone_no' => $this->call->phone_no,
                'status'   => 'waiting',
                'is_real'  => 0
            ]);
            $this->callSuccess = true;
            session()->flash('message', __('Jūsų numeris sėkmingai užregistruotas, susisieksime artimiausių metų.'));

            $this->clearFields();
        }
    }

    public function clearFields()
    {
        $this->call->name = '';
        $this->call->phone_no  = '';
    }

    public function render()
    {
        return view('livewire.help-call');
    }
}
