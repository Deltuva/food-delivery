<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_no', 'name', 'address_line_1', 'address_line_2', 'city', 'post_code', 'phone_no', 'email', 'notes'];

    /**
     * Relationship Order Items - hasMany
     *
     * @return hasMany
     */
    public function order_items() {
        return $this->hasMany(OrderItem::class);
    }
}
