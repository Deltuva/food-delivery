<?php

namespace App\Models\Types;

class CallStatusType
{
    const STATUS_WAITING  = 'waiting';
    const STATUS_APPROVED = 'approved';
    const STATUS_CANCELED = 'canceled';
}