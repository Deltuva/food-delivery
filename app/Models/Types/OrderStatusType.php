<?php

namespace App\Models\Types;

class OrderStatusType
{
    const STATUS_PENDING    = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_COMPLETED  = 'completed';
    const STATUS_DECLINE    = 'decline';
}