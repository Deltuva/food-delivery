<?php

namespace App\Models\Types;

class ProductType
{
    const CATEGORY_PIZZAS  = 1;
    const CATEGORY_SUSHI   = 2;
    const CATEGORY_SALATS  = 3;
    const CATEGORY_DESERTS = 4;
    const CATEGORY_DRINKS  = 5;
}
