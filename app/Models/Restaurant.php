<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    use HasFactory;

    protected $table = 'restaurants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'address','contact_name' , 'contact_no', 'contat_email', 'work_hours', 'is_open'];

    /**
     * Relationship Products - hasMany
     *
     * @return hasMany
     */
    public function products() {
        return $this->hasMany(Product::class);
    }
}
