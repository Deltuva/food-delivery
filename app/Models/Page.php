<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    CONST PAYMENT_PAGE_SLUG = 'mokejimai';
    CONST ABOUT_PAGE_SLUG   = 'apie-mus';
    CONST CONTACT_PAGE_SLUG = 'kontaktai';

    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Scope Active page
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', TRUE);
    }
}
