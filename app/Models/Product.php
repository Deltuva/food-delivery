<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const PER_PAGE = 8;

    protected $table = 'products';

    protected $fillable = [];

    public function categoryId()
    {
        return $this->category_id;
    }
    
    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function restaurant() {
        return $this->belongsTo(Restaurant::class);
    }

    public function size() {
        return $this->belongsTo(Size::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', FALSE);
    }

    public function photoImage()
    {
        if (!is_null($this->image)) {
            return asset('/storage/products/' . $this->image);
        }
    }

    public function getOriginalPrice(){
        return $this->original_price;
    }

    public function isDiscounted(){
        return $this->discount_price > 0 ? $this->discount_price : '';
    }

    public function getDiscountForPrice(){
        return $this->original_price - $this->discount_price;
    }

    public function isRecommend(){
        return $this->is_top === 1;
    }
}
