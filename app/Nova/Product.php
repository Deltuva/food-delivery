<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Boolean;
use Vyuldashev\NovaMoneyField\Money;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Product::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title', 'description'
    ];

    /**
     * The icon of the resource.
     *
     * @return string
     */
    public static function icon()
    {
        return '<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path fill="var(--sidebar-icon)" d="M4.06 13a8 8 0 0 0 5.18 .... e.t.c."/>
            </svg>';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Produktai';

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Produktai');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Produktas');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')
                ->sortable(),

            BelongsTo::make('Restoranas', 'restaurant', Restaurant::class)
                ->hideFromIndex()
                ->sortable()
                ->rules('required'),

            Image::make('Image', 'image')
                ->store(function (Request $request, $model) {
                    return (new Hooks\StoreMedia)
                        ->storeImage($request, 'products', 'image');
                })
                ->disk('products')
                ->maxWidth(150)
                ->storeOriginalName('image')
                ->prunable(),

            Text::make('Pavadinimas', 'title')
                ->sortable()
                ->rules('required', 'max:255')
                ->creationRules('unique:products,title')
                ->updateRules('unique:products,title,{{resourceId}}'),

            Textarea::make('Aprašymas', 'description')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:255'),

            Money::make('Original price', 'EUR')
                ->sortable()
                ->rules('required'),
            Money::make('Discount price', 'EUR')
                ->sortable()
                ->rules('required'),

            BelongsTo::make('Dydis', 'size', Size::class)
                ->sortable()
                ->rules('required'),

            Boolean::make('Yra', 'in_stock'),
            Boolean::make('Perkamiausia', 'is_top')
            ->hideFromIndex(),

            BelongsTo::make('Kategorija', 'category', Category::class)
                ->sortable()
                ->rules('required'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new Metrics\Product\CountProducts,
            new Metrics\Product\CountNewProductsTrend,
            new Metrics\Product\CountProductsRevenue,
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\Product\ProductRestaurantFilter,
            new Filters\Product\ProductCategoryFilter,
            new Filters\Product\ProductSizeFilter,
            new Filters\Product\ProductPriceFilter
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
