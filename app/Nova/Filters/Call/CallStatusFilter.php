<?php

namespace App\Nova\Filters\Call;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Models\Types\CallStatusType;

class CallStatusFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The displayable name of the filter.
     *
     * @var string
     */
    public $name = 'Skambučių filtras';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('status', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            __('Laukiamami')  => CallStatusType::STATUS_WAITING,
            __('Patvirtinti') => CallStatusType::STATUS_APPROVED,
            __('Atšaukti')    => CallStatusType::STATUS_CANCELED
        ];
    }
}
