<?php

namespace App\Nova\Filters\Product;

use App\Models\Size;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class ProductSizeFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The displayable name of the filter.
     *
     * @var string
     */
    public $name = 'Dydžių filtras';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('size_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $sizeModel = new Size;
        $sizes = $sizeModel->all();

        $sizesNewData = [];
        foreach ($sizes as $sizex) {
            if ($sizex->size) {
                $sizesNewData[$sizex->id]['name'] = "{$sizex->size} ({$sizex->category->name})";
            }
        }
        return $sizesNewData;
    }
}
