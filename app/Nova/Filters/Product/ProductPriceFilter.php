<?php

namespace App\Nova\Filters\Product;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class ProductPriceFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The displayable name of the filter.
     *
     * @var string
     */
    public $name = 'Kainų filtras';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('original_price','>', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            'Produktai brangesni nei > 1' => __('1'),
            'Produktai brangesni nei > 10' => __('10'),
            'Produktai brangesni nei > 100' => __('100'),
        ];
    }
}
