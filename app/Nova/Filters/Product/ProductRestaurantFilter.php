<?php

namespace App\Nova\Filters\Product;

use App\Models\Restaurant;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class ProductRestaurantFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The displayable name of the filter.
     *
     * @var string
     */
    public $name = 'Restoranų filtras';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('restaurant_id', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $restaurantModel = new Restaurant;
        $restaurants = $restaurantModel->all();

        $restaurantsNewData = [];
        foreach ($restaurants as $restaurant) {
            if ($restaurant->name) {
                $restaurantsNewData[$restaurant->id]['name'] = "{$restaurant->name} ($restaurant->address)";
            }
        }
        return $restaurantsNewData;
    }
}
