<?php

namespace App\Nova\Filters\Order;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Models\Types\OrderStatusType;

class OrderStatusFilter extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * The displayable name of the filter.
     *
     * @var string
     */
    public $name = 'Užsakymų filtras';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where('status', $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            __('Laukiama')  => OrderStatusType::STATUS_PENDING,
            __('Vykdomas')  => OrderStatusType::STATUS_PROCESSING,
            __('Užbaigtas') => OrderStatusType::STATUS_COMPLETED,
            __('Atšauktas') => OrderStatusType::STATUS_DECLINE
        ];
    }
}
