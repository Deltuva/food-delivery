<?php

namespace App\Nova\Filters\Order;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\BooleanFilter;

class OrderIsPaidBooleanFilter extends BooleanFilter
{
    /**
     * The displayable name of the filter.
     *
     * @var string
     */
    public $name = 'Apmokėtų užsakymų filtras';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        if ($value['paid'])
            $query->where('is_paid', $value['paid']);

        return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            __('Apmokėti užsakymai') => 'paid'
        ];
    }

    /**
     * The default boolean value
     *
     * @var boolean
     */
    public function default()
    {
        return [
            'paid' => false
        ];
    }
}
