<?php

namespace App\Nova\Metrics\Product;

use App\Models\Product;
use Laravel\Nova\Metrics\Value;
use Laravel\Nova\Http\Requests\NovaRequest;

class CountProductsRevenue extends Value
{
    /**
     * The displayable name of the metric.
     *
     * @var string
     */
    public $name = 'Iš viso produktų už';

    /**
     * Calculate the value of the metric.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return mixed
     */
    public function calculate(NovaRequest $request)
    {
        return $this->sum($request, Product::class, 'original_price')
            ->prefix('€');
    }

    /**
     * Get the ranges available for the metric.
     *
     * @return array
     */
    public function ranges()
    {
        return [
            'ALL'   => __('Viso'),
            30      => __('30 d'),
            60      => __('60 d'),
            365     => __('365 d'),
            'TODAY' => __('Šiandien'),
        ];
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        // return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'product-count-products-revenue';
    }
}
