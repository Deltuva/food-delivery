<?php

namespace App\Nova\Hooks;

class StoreMedia
{
   public function storeImage($request, $dir, $name) {
        $filename = time().'.'.$request->$name->getClientOriginalName();
        $request->$name->move(public_path("storage/{$dir}"), $filename);

        return [
            $name => $filename
        ];
   }
}
