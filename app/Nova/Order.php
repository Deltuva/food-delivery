<?php

namespace App\Nova;

use Laravel\Nova\Panel;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\Textarea;
use Vyuldashev\NovaMoneyField\Money;
use Laravel\Nova\Http\Requests\NovaRequest;

class Order extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Order::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * The icon of the resource.
     *
     * @return string
     */
    public static function icon()
    {
        return '<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path fill="var(--sidebar-icon)" d="M4.06 13a8 8 0 0 0 5.18 .... e.t.c."/>
            </svg>';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Mokėjimai';

    /**
     * Disable Create button
     *
     * @return boolean
     */
    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Užsakymai');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Užsakymas');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('Užsakymo nr'), 'id')->sortable(),

            Text::make('Užsakymo kodas', 'order_no')
                ->showOnIndex()
                ->rules('required'),

            new Panel('Gavėjo informacija', $this->addressFields()),

            Status::make('Statusas', 'status')
                ->hideFromDetail()
                ->loadingWhen([
                    'pending'
                ])
                ->failedWhen(['decline']),

            Heading::make('Apmokėto užsakymo patvirtinimas'),

            Select::make('Apmokėjimo tipas', 'payment_method')
                ->hideFromIndex()
                ->rules('required')
                ->options([
                    'cash_on_delivery' => __('Gryni pinigai pristatymo metu')
                ]),

            Money::make('Total', 'EUR'),
            
            Number::make('Prekių kiekis', 'item_count'),

            Boolean::make('Apmokėtas', 'is_paid'),
        ];
    }

    /**
     * Get the address fields for the resource.
     *
     * @return array
     */
    protected function addressFields()
    {
        return [
            Text::make('Vardas', 'name')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:50'),
            Place::make('Adresas', 'address_line_1')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make('Adresas 2', 'address_line_2')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make('Miestas', 'city')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:50'),
            Text::make('Pašto kodas', 'post_code')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:15'),
            Text::make('El.paštas', 'email')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:255'),
            Heading::make('Komentaras'),

            Textarea::make('Žinutė', 'notes'),

            Heading::make('Užsakymo pateikimo data'),

            Date::make('Sukurta', 'created_at')->hideFromIndex(),

            HasMany::make('Užsakymų krepšelis', 'order_items', OrderItem::class),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new Metrics\Order\CountOrders,
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\Order\OrderStatusFilter,
            new Filters\Order\OrderIsPaidBooleanFilter,
            new Filters\Order\OrderByDateFilter,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
