<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Http\Requests\NovaRequest;

class Call extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Call::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name', 'phone_no'
    ];

    /**
     * The icon of the resource.
     *
     * @return string
     */
    public static function icon()
    {
        return '<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path fill="var(--sidebar-icon)" d="M4.06 13a8 8 0 0 0 5.18 .... e.t.c."/>
            </svg>';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Pagalba';

    /**
     * Disable Create button
     *
     * @return boolean
     */
    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Skambučiai');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Skambutis');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make('Vardas', 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Telefonas', 'phone_no')
                ->rules('required', 'max:25'),

            Status::make('Statusas', 'status')
                ->hideFromDetail()
                ->loadingWhen([
                    'waiting'
                ])
                ->failedWhen(['canceled']),

            Select::make('Statusas', 'status')
                ->hideFromIndex()
                ->hideFromDetail()
                ->rules('required')
                ->options([
                    'waiting'  => 'Laukiama',
                    'approved' => 'Patvirtinta',
                    'canceled' => 'Atšaukta',
                ]),

            Boolean::make('Tikras', 'is_real'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new Filters\Call\CallStatusFilter,
            new Filters\Call\CallByDaysFilter,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
