<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Http\Requests\NovaRequest;

class Setting extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Setting::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * The icon of the resource.
     *
     * @return string
     */
    public static function icon()
    {
        return '<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path fill="var(--sidebar-icon)" d="M4.06 13a8 8 0 0 0 5.18 .... e.t.c."/>
    </svg>';
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Svetainė';

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('Nustatymai');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('Nustatymas');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make('Nustatymas', 'name')
                ->sortable()
                ->rules('required', 'max:50'),

            Text::make('Raktažodis', 'key')
                ->hideFromIndex()
                ->sortable()
                ->rules('required', 'max:50'),

            Text::make('Matoma svetainėjė reikšmė', 'value')
                ->hideFromIndex()
                ->showOnDetail()
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Matoma', function () {
                $name = $this->name;
                $value = $this->value;
                $src = asset('images/' . $value);

                $image = [
                    'width'  => 100,
                    'height' => 100,
                    'name'   => $name,
                    'src'    => $src,
                ];
                
                if (!empty($this->value)) {
                    if ($this->type == 'image') {
                        if (file_exists('images/' . $value)) {
                            return "<img src='{$image['src']}' width='{$image['width']}' height='{$image['height']}' alt='{$image['name']}'>";
                        } else {
                            return __('Paveiksliuko nepavyko rasti.');
                        }
                    } else {
                        return $value;
                    }
                }
            })
                ->hideFromDetail()
                ->asHtml(),


            Trix::make('Aprašymas', 'description'),

            Select::make('Tipas', 'type')
                ->hideFromIndex()
                ->rules('required')
                ->options([
                    'string'  => 'Tekstas',
                    'image'   => 'Paveikslėlis',
                    'integer' => 'Skaičius',
                    'select'  => 'Sąrašas meniu',
                    'editor'  => 'Editorius',
                    'youtube' => 'Youtubė',
                ]),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
