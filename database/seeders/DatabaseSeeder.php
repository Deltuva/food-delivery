<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Categories
        $this->call(CategorySeeder::class);
        $this->command->info("Categories table seeded.");

        // Restaurants
        $this->call(RestaurantSeeder::class);
        $this->command->info("Restaurants table seeded.");
    }
}
