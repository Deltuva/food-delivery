<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Restaurant;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Restaurant::create([
            'name'          => 'FoodDelivery',
            'address'       => 'Vilnius, Ukmerges g.14',
            'contact_name'  => 'Mindaugas Deltuva',
            'contact_no'    => '+37064475344',
            'contact_email' => 'deltuva.mindaugas@gmail.com',
            'work_hours'    => '8val - 16val',
            'is_open'       => false,
        ]);
    }
}
